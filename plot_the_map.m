%Computes and plots the probability of the vehicle position from X(NUMLOG)
%measurement and computes RMS

clear, close all
me = mfilename;
mydir = which(me); mydir = mydir(1:end-2-numel(me)); 
addpath(genpath(mydir)); 

insorob = 'insorob20162.csv';           %wifilog
wifilog = 'wifilog20162.csv';           %insorob for given wifilog

T = readtable(wifilog,'Delimiter',',','ReadVariableNames',false);
A = importdata(insorob,',');
load('Map20163.mat');           %created location GP map
load('inso20162.mat');          %corrected inso for given wifilog


NUMLOG = 30;                    %number of measurements to compute the probability from
XRange = 15;                     %area around X
YRange = XRange;                %area around Y
ZRange = 2;                   %area around Z


%% 
RSSI = table2array(T(:,3));

% plot original inso
meanfunc = @meanZero;
covfunc = @covSEard;
likfunc = @likGauss;
[Xtest1, Xtest2, Xtest3] = meshgrid(-20:1:30, -20:1:30,-5:1:5);
Xtest = [Xtest1(:) Xtest2(:) Xtest3(:)];
%%

for j = 46 %1:size(out.MAC)% over all known AP MACs
    % evaluate the probability p(RSSI|Xtest)
    x= [out.data(j).x,out.data(j).y, out.data(j).z];
    rssi= out.data(j).rssi;
    hyp=out.hyp(j);          
    [ymu, ys2, fmu, fs2] = gp(hyp, @infExact, meanfunc, covfunc, likfunc, x, rssi, Xtest);
    ymu = ymu - 100;

    %plot the mean map for article
    figure(j);
    cla();

    
    contour_lines = -115:2:-65;
    C1 = subplot(1,5,1);
    ymu3d = reshape(ymu,size(Xtest1));
    contourf(reshape(Xtest1(:,:,1),[51,51]),...
            reshape(Xtest2(:,:,1),[51,51]),...
            reshape(ymu3d(:,:,1),[51,51]),contour_lines);
    axis equal;hold on;xlabel('x[m]');ylabel('y[m]');title('z = -5m')
    colormap(jet);    
    data_z_cut = abs(out.data(j).z+5)<2.5;
    plot3(out.data(j).x(data_z_cut),out.data(j).y(data_z_cut), zeros(size(out.data(j).x(data_z_cut))), 'k*','MarkerSize',7)
    
    
    C2 = subplot(1,5,2);
    ymu3d = reshape(ymu,size(Xtest1));
    contourf(reshape(Xtest1(:,:,6),[51,51]),...
            reshape(Xtest2(:,:,6),[51,51]),...
            reshape(ymu3d(:,:,6),[51,51]),contour_lines);
    colormap(jet);
    axis equal;hold on;xlabel('x[m]');ylabel('y[m]');title('z = 0m')
    data_z_cut = abs(out.data(j).z)<2.5;
    plot3(out.data(j).x(data_z_cut),out.data(j).y(data_z_cut),  zeros(size(out.data(j).x(data_z_cut))), 'k*','MarkerSize',7)
    %clabel(C);
    
    C3 = subplot(1,5,3);
    ymu3d = reshape(ymu,size(Xtest1));
    contourf(reshape(Xtest1(:,:,11),[51,51]),...
            reshape(Xtest2(:,:,11),[51,51]),...
            reshape(ymu3d(:,:,11),[51,51]),contour_lines);
    colormap(jet);
    axis equal;hold on;xlabel('x[m]');ylabel('y[m]');title('z = 5m')
    data_z_cut = abs(out.data(j).z-5)<2.5;
    plot3(out.data(j).x(data_z_cut),out.data(j).y(data_z_cut),  zeros(size(out.data(j).x(data_z_cut))), 'k*','MarkerSize',7)
    %clabel(C)    
    colorbar;
    
    set([C1 C2 C3],'clim',[-115 -65]);
    
    
    
    C3 = subplot(1,5,4:5);
    plot3(INSO(:,2),INSO(:,3),INSO(:,4),'b');hold on; grid on;
    plot3(out.data(j).x(:)+1,out.data(j).y(:), out.data(j).z(:),'k*');
    plot3([-15,22],[23, 23],[-5, -5],'k-.');
    plot3([-15,22],[23, 23],[0, 0],'k-.');
    plot3([-15,22],[23, 23],[5, 5],'k-.');
    xlabel('x'), ylabel('y'), zlabel('z'); axis equal;
    legend('Mapping sortie trajectory','Wi-Fi signal measurements');
    
        
    drawnow;


end

