function WIFILOGOUT = filterMAC(wifilog,param)

WIFILOG = readtable(wifilog,'Delimiter',',','ReadVariableNames',false);
MAC = WIFILOG{:,{'Var2'}};
MACS = sort(unique(WIFILOG{:,{'Var2'}}));
MACF = zeros(length(MACS),1);

%counting occurencies
for i=1:length(MAC)
   for j=1:length(MACS)
      if strcmp(MAC(i), MACS(j))
            MACF(j)= MACF(j)+1;
          break;
      end    
   end
end

%filtering MACs with occurencies less than %param
c = 1;
for i=1:length(MACF)
   if MACF(i)>= param
      MACOUT(c,1)= MACS(i);
      c=c+1;
   end
end

%Wifilog without low occurence MACs
c = 1;
for i=1:length(MAC)
   for j=1:length(MACOUT)       
       if strcmp(MAC(i), MACOUT(j))
            WIFILOGOUT(c,:)= WIFILOG(i,:);
            c=c+1;
          break;
      end    
   end
end

