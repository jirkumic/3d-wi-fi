function [state] = lowVarianceRS(prev_state, weight, state_size)
    weight = weight/sum(weight);
    state = zeros(1,state_size);    % Initialize empty final state
    r = rand*(state_size^-1);       % Select random number between 0-M^-1
    w = weight(1);                  % Initial weight
    i = 1;
    j = 1;

    for m = 1:state_size
        U = r + (m - 1)/state_size; % Index of original sample + size^-1
        while U > w                 % I'm not sure what this loop is doing
            i = i + 1;
            w = w + weight(i);
        end
        state(j) = prev_state(i);   % Add selected sample to resampled array
        j = j + 1;
    end
end
