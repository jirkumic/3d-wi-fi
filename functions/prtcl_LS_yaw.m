function[yawDiff, add] = prtcl_LS_yaw(directionM, yawPrev, linePos, )

% clear all;
% close all;
% directionM = [2.5,3;1.1,1.8];
% yawPrev = 3;
% LSKONST = 1;
% linePos =2;

endPos = linePos;
startPos = 1;

%just LeastSquare method
y=0;xy=0;x=0;x2=0;
if length(directionM) < 2
    printf('Error: Give at least 2 vectors.');
    return
else
    for i=1:length(directionM); 
       y = y+directionM(i,2);
       x = x+directionM(i,1);
       xy = xy + directionM(i,2)*directionM(i,1);
       x2 = x2 + directionM(i,1)^2;
    end
end

a = (y*x2-x*xy)/(length(directionM)*x2-x^2);                 %LeastS param; y= a + bx
b = (length(directionM)*xy-x*y)/(length(directionM)*x2-x^2); %LeastS direction param

yStart = directionM(startPos,1)*b + a;
yEnd = directionM(endPos,1)*b + a;
yYaw = yEnd-yStart;
xYaw = directionM(endPos,1)-directionM(startPos,1);

%if the robot does not move
if sqrt(yYaw^2+xYaw^2) < 0.5
    add = 0;
    yawDiff = 0;
    return;
end

%if the angle is 90 or -90 degree
if isnan(b)
    if yawPrev < 0
        yaw = -pi/2;
    else
        yaw = pi/2;
    end
else
   yaw = atan2(yYaw, xYaw);
end

yawDiff =  yawPrev -yaw;
if yawDiff > pi
    yawDiff = yawDiff -2*pi;
elseif yawDiff < -pi
    yawDiff = yawDiff +2*pi;
end


%swap coords (optional)

% if yawDiff > 0
%     yawDiff = yawDiff-pi;
% elseif yawDiff < 0
%     yawDiff = yawDiff +pi;
% end


%if the robot is going straight
if yawDiff < pi/5 && yawDiff > -pi/5
    add = 1;
else
    add = 0;
end

% plot(directionM(:,1),directionM(:,2),'b*')
% X=-5:1:5;
% Y=b*X + a;
% hold on;
% grid on;
% plot(X,Y,'r')
% plot(xPrev,yPrev,'g*')
% plot(xYaw,yYaw,'k*')
