function [ prior_map ] = generate_prior_map( map_path )
%GENERATE_PRIOR_MAP looks where the robot went when mapping ans sets the
%grid


x_span = -20:25;
y_span = -5:25;
z_span = -6:0.2:6;

x_box = 2;
y_box = 2;
z_box_up = 2;
z_box_down = 2;


[x_coords, y_coords, z_coords] = meshgrid(x_span,y_span,z_span);

prior_map.grid = zeros(size(x_span,2),size(y_span,2),size(z_span,2));

for i = 1:size(map_path,1)
    [~,x_index] = min(abs(x_span - map_path(i,1)));
    [~,y_index] = min(abs(y_span - map_path(i,2)));
    [~,z_index] = min(abs(z_span - map_path(i,3)));
    
    mark_x = (x_index-x_box):(x_index+x_box);
    mark_y = (y_index-y_box):(y_index+y_box);
    mark_z = (z_index-z_box_down):(z_index+z_box_up);
    
    mark_x(mark_x<1) = 1;
    mark_x(mark_x>size(x_span,2)) = size(x_span,2);
    mark_y(mark_y<1) = 1;
    mark_y(mark_y>size(y_span,2)) = size(y_span,2);
    mark_z(mark_z<1) = 1;
    mark_z(mark_z>size(z_span,2)) = size(z_span,2);
    
    prior_map.grid(mark_x,mark_y,mark_z) = 1;    
        
    
end


%     % plot what we get
%     %figure();
%     %cla();
%     axis equal;
%     hold on;
%     grid on;
% for i = 1:size(x_span,2)
%     for j = 1:size(y_span,2)
%         for k = 1:size(z_span,2)
%             if prior_map.grid(i,j,k) == 1
%                 plot3(x_span(i),y_span(j),z_span(k),'ob');
%             end
%         end
%     end
% end
%     
%     

one_indices = prior_map.grid==1;
zero_indices = prior_map.grid==0;

prior_map.grid(one_indices) = log(1/sum(prior_map.grid(:)==1));
prior_map.grid(zero_indices) = -1000000;

prior_map.x_span = x_span;
prior_map.y_span = y_span;
prior_map.z_span = z_span;

end

