function [x, y, z, RSSI] = pairCoorRSSI(mac, wifilog_with_position)

j=1;
for i=1:1:length(wifilog_with_position.pos)
   if strcmp(mac, wifilog_with_position.MAC(i))
        x(j)= wifilog_with_position.pos(i,1);
        y(j)= wifilog_with_position.pos(i,2);
        z(j)= wifilog_with_position.pos(i,3);
        RSSI(j)= wifilog_with_position.RSSI(i);
        
        j=j+1;  
   end
end
    
