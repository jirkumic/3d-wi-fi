function [ prior_constraint_log ] = get_prior_constraint_log( Xtest, prior_map )
%GET_PRIOR_CONSTRAINT_LOG returns vector of log probability of being in
%coordinates Xtest

prior_constraint_log = zeros(size(Xtest,1),1);
x_span = prior_map.x_span;
y_span = prior_map.y_span;
z_span = prior_map.z_span;


for i = 1:size(Xtest,1)
   
    [~,x_index] = min(abs(x_span - Xtest(i,1)));
    [~,y_index] = min(abs(y_span - Xtest(i,2)));
    [~,z_index] = min(abs(z_span - Xtest(i,3)));
    
    prior_constraint_log(i) = prior_map.grid(x_index,y_index,z_index);
    
end


end

