%Computes and plots the probability of the vehicle position from X(NUMLOG)
%measurement and computes RMS

clear, close all
me = mfilename;
mydir = which(me); mydir = mydir(1:end-2-numel(me)); 
addpath(genpath(mydir)); 

wifilog = 'wifilog20151.csv';           %wifilog
insorob = 'insorob20151.csv';           %insorob for given wifilog

T = readtable(wifilog,'Delimiter',',','ReadVariableNames',false);
A = importdata(insorob,',');
load('Map20163&22M.mat');           %created location GP map
load('inso20151.mat');          %corrected inso for given wifilog

LSCount = 4;                    %number of corrected inso trajectory cycles for yaw computation
NUMLOG = 20;                    %number of measurements to compute the probability from
YawErrConst = 0.2;
DELTAconst= 0.1;
XRange = 8;                     %area around X
YRange = XRange;                %area around Y
ZRange = 2.5;                   %area around Z


odom_initial_prior_sigma = 100*eye(3);
odom_user_prior_sigma = 5*eye(3);   % how sharp is the prior gaussian distribution
odom_prior_sigma = odom_initial_prior_sigma;
%% Changing x and y axes directions (If needed)


INSO(:,2) = -INSO(:,2);
INSO(:,3) = -INSO(:,3);

A(:,2) = -A(:,2);
A(:,3) = -A(:,3);

%%
a = A(:,2);
b = A(:,3);
c = A(:,4);

RSSI = table2array(T(:,3));

% plot original inso
figure(1);
% plot3(a,b,c,'g')%ODOM
hold on; grid on; axis equal;
plot3(INSO(:,2),INSO(:,3),INSO(:,4),'b')  % ICP
xlabel('X-axis [m]');
ylabel('Y-axis [m]');
zlabel('Z-axis [m]');
meanfunc = @meanZero;
covfunc = @covSEard;
likfunc = @likGauss;

size1=-15:1:25;
size2=-10:1:25;
size3=-5:0.2:5;
tempLp = 0;
count=0;
insopointer=1;
insoC= zeros(length(A)-1,4);
delta = zeros(1,3);

pos= [14.7437182892763,-1.42816618501877,0.224526173469880]; 
yawDiff = 0;
yawErr = 0;
old_eul = [0,0,0];
DirCount = 1;
Count = 1;
CErr=0; %

INSO_DIFF_B = zeros(size(A,1),3);
%get inso distance increments 
for i=2:length(A)
    INSO_DIFF_M = A(i,2:4)-A(i-1,2:4);
    INSO_DIFF_B(i,:) = quat_2_dcm(A(i-1,5:8)')*INSO_DIFF_M';
end
%%

%%generate prior constraint map
% prior_map = generate_prior_map([INSO2.INSO(:,2),INSO2.INSO(:,3),INSO2.INSO(:,4)]);
maximums = [];
deltas = [];

load('ws.mat');
[Xtest1, Xtest2, Xtest3] = meshgrid(-15:1:25,-10:1:25,-5:0.2:5);
Xtest = [Xtest1(:) Xtest2(:) Xtest3(:)];
tempLp=0;
count=4260;
INSO2=load('inso201621.mat');
prior_map = generate_prior_map([INSO2.INSO(:,2),INSO2.INSO(:,3),INSO2.INSO(:,4)]);

%%
for i=4850:size(T)  % over all wifi packets
    for j=1:size(out.MAC)  % over all known AP MACs
        if strcmp(T{i,{'Var2'}},out.MAC(j))  %if given packet matches MAC
            formatSpec = '%d out of %d, MAC %d \n';
            fprintf(formatSpec,i,size(T),j);
            count = count + 1;               % how many packets do we have at the moment
            % evaluate the probability p(RSSI|Xtest)
            x= [out.data(j).x,out.data(j).y, out.data(j).z];
            rssi= out.data(j).rssi;
            Ytest(1:size(Xtest,1))= RSSI(i)+100;
            hyp=out.hyp(j);          
            [ymu, ys2, fmu, fs2, lp] = gp(hyp, @infExact, meanfunc, covfunc, likfunc, x, rssi, Xtest,Ytest.');
            
%             figure(1)           
                      
            o1=1;o2=1;o3=1;
            for o=1:length(lp)
                if Xtest(o,3)== -5
                    v1(o1)=lp(o);
                    o1=o1+1;
                elseif Xtest(o,3)== 0
                    v2(o2)=lp(o);
                    o2=o2+1;
                elseif Xtest(o,3)== 5
                    v3(o3)=lp(o);
                    o3=o3+1;
                end
            end
            
            contour_lines = -15:0.2:0;
            C1 = subplot(1,5,1);
            contourf(size1,size2,reshape(v1,[length(size2),length(size1)]),contour_lines);
            axis equal;hold on;xlabel('x[m]');ylabel('y[m]');title('z = -5m')
            colormap(jet);    
            colo1=colorbar('southoutside');
            colo1.Label.String = 'Log probability';
            
            C2 = subplot(1,5,2);
            contourf(size1,size2,reshape(v2,[length(size2),length(size1)]),contour_lines);
            colormap(jet);
            axis equal;hold on;xlabel('x[m]');ylabel('y[m]');title('z = 0m')
            colo2=colorbar('southoutside');
            colo2.Label.String = 'Log probability';
            
            C3 = subplot(1,5,3);
            contourf(size1,size2,reshape(v3,[length(size2),length(size1)]),contour_lines);
            colormap(jet);
            axis equal;hold on;xlabel('x[m]');ylabel('y[m]');title('z = 5m')  
            colo3=colorbar('southoutside');
            colo3.Label.String = 'Log probability';   
            set([C1 C2 C3],'clim',[-15 0]);
            
            subplot(1,5,4:5);
            plot3(INSO(:,2),INSO(:,3),INSO(:,4),'b');hold on; grid on;
            plot3(insoC(1:k,2),insoC(1:k,3),insoC(1:k,4),'k');
            view(45,35)
            xlabel('x [m]'), ylabel('y [m]'), zlabel('z [m]'); axis equal;
            legend('Ground truth','Proposed WiFi localization');           
            
            subplot(1,5,1:5);  
            cla();
            % accumulate Lp
            tempLp=tempLp+lp;
            
            o1=1;o2=1;o3=1;
            for o=1:size(tempLp)
                if Xtest(o,3)== -5
                    v1(o1)=tempLp(o);
                    o1=o1+1;
                elseif Xtest(o,3)== 0
                    v2(o2)=tempLp(o);
                    o2=o2+1;
                elseif Xtest(o,3)== 5
                    v3(o3)=tempLp(o);
                    o3=o3+1;
                end
            end
            
            contour_lines = -116:2:0;
            C1 = subplot(1,5,1);
            contourf(size1,size2,reshape(v1,[length(size2),length(size1)]),contour_lines);
            axis equal;hold on;xlabel('x[m]');ylabel('y[m]');title('z = -5m')
            colormap(jet);    
            colo1=colorbar('southoutside');
            colo1.Label.String = 'Log probability';
            
            C2 = subplot(1,5,2);
            contourf(size1,size2,reshape(v2,[length(size2),length(size1)]),contour_lines);
            colormap(jet);
            axis equal;hold on;xlabel('x[m]');ylabel('y[m]');title('z = 0m')
            colo2=colorbar('southoutside');
            colo2.Label.String = 'Log probability';
            
            C3 = subplot(1,5,3);
            contourf(size1,size2,reshape(v3,[length(size2),length(size1)]),contour_lines);
            colormap(jet);
            axis equal;hold on;xlabel('x[m]');ylabel('y[m]');title('z = 5m')  
            colo3=colorbar('southoutside');
            colo3.Label.String = 'Log probability';   
            set([C1 C2 C3],'clim',[-116 0]);
            
            subplot(1,5,4:5);
            plot3(INSO(:,2),INSO(:,3),INSO(:,4),'b');hold on; grid on;
            plot3(insoC(1:k,2),insoC(1:k,3),insoC(1:k,4),'k');
            view(45,35)
            xlabel('x [m]'), ylabel('y [m]'), zlabel('z [m]'); axis equal;
            legend('Ground truth','Proposed WiFi localization'); 
            subplot(1,5,1:5);  
            cla();     
            % if enough packets already, use them
            if mod(count,NUMLOG) == 0
                % plot the probability
%                 figure(2);
%                 cla();
%                 
%                 scatter_index = tempLp(:)>quantile(tempLp(:),0.9);
%                 scatter3(Xtest1(scatter_index), Xtest2(scatter_index), Xtest3(scatter_index),...
%                      [], exp(tempLp(scatter_index)/max(abs(tempLp))) );
%                 axis equal;
%                 hold on;
%                  
                                
                
                n= count/NUMLOG;
                % find the maximum of tempLp
                [M,I] = max(tempLp);
                X(n,1) = Xtest(I,1);
                Y(n,1) = Xtest(I,2);
                Z(n,1) = Xtest(I,3);
                innovation_distribution = tempLp;
                tempLp = 0;
                
                
%                 figure(2);
%                 plot3(X(n,1),Y(n,1),Z(n,1),'    lol = sqrt(yYaw^2+xYaw^2)m*','MarkerSize',10);
%                 drawnow;
                              
                
                figure(1);
                grid on;
                hold on;
%                 plot3(X(n,1),Y(n,1),Z(n,1),'r*');
                drawnow;
                
                %% Inso correction and RMS error
                for k=insopointer+1:1:length(A)-1
                    % current correcting wifi pos time
                    tmpT = T{i,{'Var1'}};
                    
                    %avoid  false setting of k
                    if tmpT < A(1,1);
                        continue;
                    end
                    
                    %next position according to distance increament and
                    %added yaw error
                    if k==1
                        pos= A(k,2:4);
                    else
                        old_eul = quat_2_euler(A(k,5:8)');                                        
                        new_eul = old_eul + [ 0 ; 0; yawErr];
                        
                        if new_eul(3) > pi
                            new_eul(3) = new_eul(3) - 2*pi;
                        elseif yawErr < -pi
                            new_eul(3) = new_eul(3) + 2*pi;
                        end
                        
                        pos = pos + ...
                            (quat_2_dcm(euler_2_quat(new_eul))'*INSO_DIFF_B(k,:)')';
                      
                    end
                    
                    % copy original
                    insoC(k,1)=A(k,1);
                    % correct by last delta
                    insoC(k,2:4)= pos-delta(1,1:3);
                    
                    %save x and y pos for computating the yaw
                    Direction(DirCount,1) = insoC(k,2);
                    Direction(DirCount,2) = insoC(k,3);
                    DirCount = DirCount + 1;

                    % if reached wifi time, compute new delta and brake
                    if A(k+1,1) >= tmpT &&  A(k,1) <= tmpT
                        
                        %compute yaw error from corrected inso from last
                        % X(TestLSCount) loops
                        if Count < LSCount
                            Count = Count + 1;
                        else
                            Count = 0;
                            [yawDiff, add] = leastSquareYaw(Direction,new_eul(3),DirCount-1);
                            DirCount = 1;
                            clearvars Direction;
                            
                            if add ==1
                                %difference between new yaw and inso yaw                              
                                yawErr = yawErr - yawDiff*YawErrConst;
                                if yawErr > pi
                                    yawErr = yawErr -2*pi;
                                elseif yawErr < -pi
                                    yawErr = yawErr +2*pi;
                                end

                                CErr = CErr + 1;
                                YAWERR(CErr)= yawErr;
                                
                            end
                            
                        end
                                                
                        % the prior mean
                        odom_prior_mu = insoC(k,2:4);
                        
                        % the prior distribution
                        prior_dist = log(mvnpdf_non_lic(Xtest,odom_prior_mu,odom_prior_sigma));
                        
                        cla();
                        plot3(INSO(:,2),INSO(:,3),INSO(:,4),'b')
                        plot3(insoC(1:k,2),insoC(1:k,3),insoC(1:k,4),'k');
                        
                        o1=1;o2=1;o3=1;
                        for o=1:size(prior_dist)
                            if Xtest(o,3)== -5
                                v1(o1)=prior_dist(o);
                                o1=o1+1;
                            elseif Xtest(o,3)== 0
                                v2(o2)=prior_dist(o);
                                o2=o2+1;
                            elseif Xtest(o,3)== 5
                                v3(o3)=prior_dist(o);
                                o3=o3+1;
                            end
                        end

                        contour_lines = -116:2:0;
                        C1 = subplot(1,5,1);
                        contourf(size1,size2,reshape(v1,[length(size2),length(size1)]),contour_lines);
                        axis equal;hold on;xlabel('x[m]');ylabel('y[m]');title('z = -5m')
                        colormap(jet);    
                        colo1=colorbar('southoutside');
                        colo1.Label.String = 'Log probability';

                        C2 = subplot(1,5,2);
                        contourf(size1,size2,reshape(v2,[length(size2),length(size1)]),contour_lines);
                        colormap(jet);
                        axis equal;hold on;xlabel('x[m]');ylabel('y[m]');title('z = 0m')
                        colo2=colorbar('southoutside');
                        colo2.Label.String = 'Log probability';

                        C3 = subplot(1,5,3);
                        contourf(size1,size2,reshape(v3,[length(size2),length(size1)]),contour_lines);
                        colormap(jet);
                        axis equal;hold on;xlabel('x[m]');ylabel('y[m]');title('z = 5m')  
                        colo3=colorbar('southoutside');
                        colo3.Label.String = 'Log probability';   
                        set([C1 C2 C3],'clim',[-116 0]);

                        subplot(1,5,4:5);
                        plot3(INSO(:,2),INSO(:,3),INSO(:,4),'b');hold on; grid on;
                        plot3(insoC(1:k,2),insoC(1:k,3),insoC(1:k,4),'k');
                        view(45,35)
                        xlabel('x [m]'), ylabel('y [m]'), zlabel('z [m]'); axis equal;
                        legend('Ground truth','Proposed WiFi localization');   
                        subplot(1,5,1:5);  
                        cla();
                        
                        % overwrite the initial non-informative sigma by
                        % user-set one
                        odom_prior_sigma = odom_user_prior_sigma;
                        
                        % compute the constraint prior
                        prior_constraint_dist = get_prior_constraint_log( Xtest, prior_map );
                        
                        cla();
                        plot3(INSO(:,2),INSO(:,3),INSO(:,4),'b')
                        plot3(insoC(1:k,2),insoC(1:k,3),insoC(1:k,4),'k');
                       
                        o1=1;o2=1;o3=1;
                        for o=1:size(prior_constraint_dist)
                            if Xtest(o,3)== -5
                                v1(o1)=prior_constraint_dist(o);
                                o1=o1+1;
                            elseif Xtest(o,3)== 0
                                v2(o2)=prior_constraint_dist(o);
                                o2=o2+1;
                            elseif Xtest(o,3)== 5
                                v3(o3)=prior_constraint_dist(o);
                                o3=o3+1;
                            end
                        end

                        contour_lines = -116:2:0;
                        C1 = subplot(1,5,1);
                        contourf(size1,size2,reshape(v1,[length(size2),length(size1)]),contour_lines);
                        axis equal;hold on;xlabel('x[m]');ylabel('y[m]');title('z = -5m')
                        colormap(jet);    
                        colo1=colorbar('southoutside');
                        colo1.Label.String = 'Log probability';

                        C2 = subplot(1,5,2);
                        contourf(size1,size2,reshape(v2,[length(size2),length(size1)]),contour_lines);
                        colormap(jet);
                        axis equal;hold on;xlabel('x[m]');ylabel('y[m]');title('z = 0m')
                        colo2=colorbar('southoutside');
                        colo2.Label.String = 'Log probability';

                        C3 = subplot(1,5,3);
                        contourf(size1,size2,reshape(v3,[length(size2),length(size1)]),contour_lines);
                        colormap(jet);
                        axis equal;hold on;xlabel('x[m]');ylabel('y[m]');title('z = 5m')  
                        colo3=colorbar('southoutside');
                        colo3.Label.String = 'Log probability';   
                        set([C1 C2 C3],'clim',[-116 0]);

                        subplot(1,5,4:5);
                        plot3(INSO(:,2),INSO(:,3),INSO(:,4),'b');hold on; grid on;
                        plot3(insoC(1:k,2),insoC(1:k,3),insoC(1:k,4),'k');
                        view(45,35)
                        xlabel('x [m]'), ylabel('y [m]'), zlabel('z [m]'); axis equal;
                        legend('Ground truth','Proposed WiFi localization');   
                        subplot(1,5,1:5);  
                        cla();
                        % posterior distribution
                        posterior_dist = prior_dist + prior_constraint_dist + innovation_distribution;
                        
                        cla();
                        plot3(INSO(:,2),INSO(:,3),INSO(:,4),'b')
                        plot3(insoC(1:k,2),insoC(1:k,3),insoC(1:k,4),'k');
                        o1=1;o2=1;o3=1;
                        for o=1:size(posterior_dist)
                            if Xtest(o,3)== -5
                                v1(o1)=posterior_dist(o);
                                o1=o1+1;
                            elseif Xtest(o,3)== 0
                                v2(o2)=posterior_dist(o);
                                o2=o2+1;
                            elseif Xtest(o,3)== 5
                                v3(o3)=posterior_dist(o);
                                o3=o3+1;
                            end
                        end
 
                        contour_lines = -116:2:-60;
                        C1 = subplot(1,5,1);
                        contourf(size1,size2,reshape(v1,[length(size2),length(size1)]),contour_lines);
                        axis equal;hold on;xlabel('x[m]');ylabel('y[m]');title('z = -5m')
                        colormap(jet);    
                        colo1=colorbar('southoutside');
                        colo1.Label.String = 'Log probability';

                        C2 = subplot(1,5,2);
                        contourf(size1,size2,reshape(v2,[length(size2),length(size1)]),contour_lines);
                        colormap(jet);
                        axis equal;hold on;xlabel('x[m]');ylabel('y[m]');title('z = 0m')
                        colo2=colorbar('southoutside');
                        colo2.Label.String = 'Log probability';

                        C3 = subplot(1,5,3);
                        contourf(size1,size2,reshape(v3,[length(size2),length(size1)]),contour_lines);
                        colormap(jet);
                        axis equal;hold on;xlabel('x[m]');ylabel('y[m]');title('z = 5m')  
                        colo3=colorbar('southoutside');
                        colo3.Label.String = 'Log probability';   
                        set([C1 C2 C3],'clim',[-116 -60]);

                        subplot(1,5,4:5);
                        plot3(INSO(:,2),INSO(:,3),INSO(:,4),'b');hold on; grid on;
                        plot3(insoC(1:k,2),insoC(1:k,3),insoC(1:k,4),'k');
                        view(45,35)
                        xlabel('x [m]'), ylabel('y [m]'), zlabel('z [m]'); axis equal;
                        legend('Ground truth','Proposed WiFi localization');    
                        subplot(1,5,1:5);  
                        cla();
                        % find maximum of posterior
                        [M,I] = max(posterior_dist);
                        if M==-inf
                            M = 1;
                        end
                        maximums = [maximums;M];
                        
                        X(n,1) = Xtest(I,1);
                        Y(n,1) = Xtest(I,2);
                        Z(n,1) = Xtest(I,3);
                        
                        if(M==1)
                            disp('skipping');
                        else
                            tmpDelta = delta;
                            delta(1,1)= insoC(k,2)-X(n,1);
                            delta(1,2)= insoC(k,3)-Y(n,1);
                            delta(1,3)= insoC(k,4)-Z(n,1);
                        
                            current_delta = delta*DELTAconst;
                            deltas = [deltas; norm(current_delta)];
                            if norm(current_delta) > inf
                                delta = tmpDelta;
                            else
                                delta = tmpDelta+current_delta;
                            end
                            %delta = tmpDelta+delta*exp(M/(NUMLOG*2));
                        end
                                               
                        figure(1);
                        plot3(insoC(insopointer:k,2),insoC(insopointer:k,3),insoC(insopointer:k,4),'k');
%                         plot3(X(n,1),Y(n,1),Z(n,1),'m*');
                        drawnow;
                        
                        insopointer=k;
                        break;
                    end
                end
            end
            break;
        end
    end
end
