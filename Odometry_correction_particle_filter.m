%Computes and plots the probability of the vehicle position from X(NUM_WIFI_LOGS)
%measurement and computes RMS for position and yaw. 
%Yaw is corrected using least square method from $YAW_LS_POINTS selected particle means

clear, close all
me = mfilename;
mydir = which(me); mydir = mydir(1:end-2-numel(me)); 
addpath(genpath(mydir)); 

wifilog_name = 'wifilog201633.csv';      %wifilog
odom_file_name = 'insorob201633.csv';    %insorob/odometry for given wifilog
load('ground_truth201633.mat');          %ground truth for given wifilog and insorob/odometry
load('GP_wifi_map201622&3.mat');           %created location GP map
CHANGE_COORD = 0;                       %set to 1 if the trajectory is in wrong direction for ex. ground truth 20151, 20161
CORRECT_YAW = 1;                        %set to 1 to correct yaw or to 0 to not correct yaw

wifilog = readtable(wifilog_name,'Delimiter',',','ReadVariableNames',false);
odom = importdata(odom_file_name,',');

YAW_LS_POINTS = 4;              %number of corrected inso trajectory points for yaw estimation
NUM_WIFI_LOGS = 20;             %number of wifi RSSI measurements to compute the probability from
YAW_ERR_PARAM = 0.05;            %This parameter defines how much will be yaw error corrected based on yaw approximation
DELTA_PARAM = 0.04;             %This parameter defines how much will be odometry corrected based on position approximation
GP_RANGE_X = 8;                 %Range on X axis for position estimation 
GP_RANGE_Y = GP_RANGE_X;        %Range on Y for position estimation
GP_RANGE_Z = 2.5;               %Range on Z for position estimation

% initial values
wifilog_counter = 0;
odom_data_pointer = 1;
corrected_odom = zeros(length(odom)-1, 4);
traveled_dist = 0;
odom_dist_increament = [0;0;0];
pos = [0,0,0];

yaw_LS_counter = 1;
direction_point_counter = 1;

yaw_error = 0;
odom_eul = [0,0,0];
odom_quat = [0,0,0,0];
corrected_eul = [0,0,0];
corrected_quat = [0,0,0,0];

RMS_counter = 0;

RMS_pos_cor = 0;
RMS_pos_uncor = 0;
RMS_pos_cor_temp = 0;
RMS_pos_uncor_temp = 0;

RMS_yaw_cor = 0;
RMS_yaw_uncor = 0;
RMS_yaw_cor_temp = 0;
RMS_yaw_uncor_temp = 0;

odom_initial_prior_sigma = 100 * eye(3);
odom_user_prior_sigma = 5 * eye(3);   % how sharp is the prior gaussian distribution
odom_prior_sigma = odom_initial_prior_sigma;

% PARTICLE filter stuff:
num_prtcl = 1000; 
initial_prtcl_variance = 20;
odometry_prtcl_variance = [1; 1; 0.1]; %x,y,z

%% Changing x and y axes directions (If needed)

if CHANGE_COORD
    ground_truth(:,2) = -ground_truth(:,2);
    ground_truth(:,3) = -ground_truth(:,3);

    odom(:,2) = -odom(:,2);
    odom(:,3) = -odom(:,3);
end

odom_diff_body = zeros(size(odom,1),3);
odom_diff_world = diff(odom(:,2:4)',1,2);
icp_diff_world = diff(ground_truth(:,2:4)',1,2);

%get inso distance increments 
for i=2:length(odom)
    odom_diff_m = odom(i,2:4) - odom(i-1,2:4);  %in the world frame
    odom_diff_body(i,:) = quat_2_dcm(odom(i-1,5:8)') * odom_diff_m'; %in the body f.
end

wifilog_time = wifilog{:,{'Var1'}};

% plot original odometry and ground truth
figure(1);
plot3(odom(:,2),odom(:,3),odom(:,4),'g')
hold on; axis equal;
plot3(ground_truth(:,2),ground_truth(:,3),ground_truth(:,4),'b')
xlabel('X-axis [m]');
ylabel('Y-axis [m]');
zlabel('Z-axis [m]');
meanfunc = @meanZero;
covfunc = @covSEard;
likfunc = @likGauss;

%% generate prior constraint map
prior_map = generate_prior_map([ground_truth(:,2),ground_truth(:,3),ground_truth(:,4)]);
maximums = [];

RSSI = table2array(wifilog(:,3));

% PARTICLE filter related variables
rssi_measurements_buffer = {};
rssi_measurements_buffer_pointer = 1;

position_particles = randn([3,num_prtcl]) * initial_prtcl_variance;
position_weights = ones(1,num_prtcl);

%% MAIN LOOP
for i=1:size(wifilog,1)  % over all wifi packets
    % current wifi pos time
    packet_time = wifilog_time(i);

    % avoid  packets that arrived before odometry measurement started
    if packet_time < odom(1,1)
        continue;
    end
    for j=1:size(GP_wifi_map.MAC,1)  % over all known AP MACs
        if strcmp(wifilog{i,{'Var2'}},GP_wifi_map.MAC(j))  % if given packet matches MAC
            formatSpec = '%d out of %d, MAC %d \n';
            fprintf(formatSpec,i,size(wifilog,1),j);
            wifilog_counter = wifilog_counter + 1;               % how many packets do we have at the moment

            % Store the values to some buffer  
            rssi_measurements_buffer{rssi_measurements_buffer_pointer}.j = j;
            rssi_measurements_buffer{rssi_measurements_buffer_pointer}.Ytest = RSSI(i) + 100;
            rssi_measurements_buffer_pointer = rssi_measurements_buffer_pointer + 1;
            
            %% if enough packets already, use them
            if mod(wifilog_counter,NUM_WIFI_LOGS) == 0                                              
                % odometry correction and RMS error
                for k = odom_data_pointer+1:1:length(odom)-1                    
                    % next position according to distance increament and
                    % added yaw error
                    if k == 1
                        pos = odom(k, 2:4);
                    else
                        odom_quat = odom(k,5:8);
                        odom_eul = quat_2_euler(odom_quat');
                        corrected_eul = wrapToPi(odom_eul + [ 0 ; 0; yaw_error]);                        
                        corrected_quat = euler_2_quat(corrected_eul);
                        
                        pos = pos + ...
                            (quat_2_dcm(corrected_quat)'*odom_diff_body(k,:)')';
                     end
                    
                    % copy original time
                    corrected_odom(k,1) = odom(k,1);
                    % save last position estimation
                    corrected_odom(k,2:4) = pos;
                    traveled_dist = traveled_dist + sqrt(icp_diff_world(1,k)^2 + icp_diff_world(2,k)^2 + icp_diff_world(3,k)^2);
                    odom_dist_increament = odom_dist_increament + odom_diff_world(:,k);

                    %save x and y pos for computating the yaw
                    direction_point_vector(direction_point_counter,1) = pos(1);
                    direction_point_vector(direction_point_counter,2) = pos(2);
                    direction_point_counter = direction_point_counter + 1;
                    
                    % if reached wifi time, compute new delta and brake
                    if odom(k+1,1) >= packet_time &&  odom(k,1) <= packet_time  
                        if CORRECT_YAW                       
                            %compute yaw error from last $yaw_LS_counter corrected
                            %odometry points
                            if yaw_LS_counter < YAW_LS_POINTS
                                yaw_LS_counter = yaw_LS_counter + 1;
                            else
                                yaw_LS_counter = 0;
                                [yaw_difference, add] = leastSquareYaw(direction_point_vector, ...
                                    corrected_eul(3), CHANGE_COORD);
                                direction_point_counter = 1;
                                clearvars direction_point_vector;

                                if add
                                    %difference(yaw error) between estimated yaw and odometry yaw                              
                                    yaw_error = wrapToPi(yaw_error - yaw_difference * YAW_ERR_PARAM);
                                    
                                end                           
                            end
                        end

                        % PARTICLE: Use this to move the particles                                                                
                        position_particles = position_particles + ...
                                             repmat(odom_dist_increament,1,num_prtcl)+ ...
                                             randn([3,num_prtcl]) .* ...
                                             odometry_prtcl_variance;                                                         
                        
                        odom_dist_increament = [0;0;0];
                        
                        % PARTICLE: Go through the buffered measurements
                        % and use them to evaluate the particles
                        log_likelihoods = ones(size(position_weights'))*(1/num_prtcl);
                        for buff_ptr = 1:(rssi_measurements_buffer_pointer-1)
                            jj = rssi_measurements_buffer{buff_ptr}.j;
                            Ytest =  rssi_measurements_buffer{buff_ptr}.Ytest;
                            x = [GP_wifi_map.data(jj).x, GP_wifi_map.data(jj).y, GP_wifi_map.data(jj).z];
                            rssi = GP_wifi_map.data(jj).RSSI;
                            hyp = GP_wifi_map.hyp(jj);          
                            [ymu, ys2, fmu, fs2, lp] = ...
                            gp(hyp, @infExact, meanfunc, covfunc, likfunc, ...
                               x, rssi, position_particles', repmat(Ytest,num_prtcl,1));
                            if min(lp) < 4*mean(lp)
                                lp = ones(size(position_weights'))*(log(1/num_prtcl));
                            end
                            log_likelihoods = log_likelihoods + lp;
                        end
                        
                        % clean the buffer
                        rssi_measurements_buffer = {};
                        rssi_measurements_buffer_pointer = 1;
                        
                        % PARTICLE: log_likelihoods too negative, shifting
                        % all up by mean value, equivalent to multiplying
                        % weights by a constant (that's done anyways)
                        log_likelihoods = log_likelihoods - max(log_likelihoods,[],1);
                        
                        % the prior mean
                        odom_prior_mu = corrected_odom(k,2:4);
                        
                        % PARTICLE: the prior distribution?
                        prior_dist = log(mvnpdf_non_lic(position_particles',odom_prior_mu,odom_prior_sigma));
                                                
                        % PARTICLE: overwrite the initial non-informative sigma by
                        % user-set one ?
                        odom_prior_sigma = odom_user_prior_sigma;
                        
                        % PARTICLE: compute the constraint prior ?
                        prior_constraint_dist = get_prior_constraint_log(position_particles', prior_map );
                        
                        % posterior distribution
                        posterior_dist =  prior_constraint_dist + prior_dist + log_likelihoods;
                        
                        position_weights = exp(posterior_dist');
                        
                        % PARTICLE: resample based on the weights
                        indices_of_new_particles = ...
                            lowVarianceRS(1:num_prtcl, ...
                                          position_weights, ...
                                          num_prtcl);
                                      
                        new_particles = position_particles(:,indices_of_new_particles);
                        
                        figure(1);
                        cla();
                        plot3(odom(:,2),odom(:,3),odom(:,4),'g')
                        hold on; axis equal;
                        plot3(ground_truth(:,2),ground_truth(:,3),ground_truth(:,4),'b');  % ground_truth
                        plot3(corrected_odom(1:k,2),corrected_odom(1:k,3),corrected_odom(1:k,4),'k','LineWidth',3);
                        plot3(position_particles(1,:),position_particles(2,:),position_particles(3,:),'b*');
                        plot3(new_particles(1,:),new_particles(2,:),new_particles(3,:),'r*');
                        legend('Odometry','Ground truth', 'Corrected odometry', 'Particles', 'Selected particles')
                        drawnow;
%                         
%                         figure(4)
%                         cla();
%                         [xq,yq] = meshgrid(-20:0.1:20);
%                         z = griddata(position_particles(1,:),position_particles(2,:),posterior_dist,xq,yq,'linear');
%                         mesh(xq,yq,z)
%                         drawnow;
%                         
%                         figure(5)
%                         cla();
%                         [xq,yq] = meshgrid(-20:0.1:20);
%                         z = griddata(position_particles(1,:),position_particles(2,:),position_weights',xq,yq,'linear');
%                         mesh(xq,yq,z)
%                         drawnow;
%                         pause;
                        
                        position_particle_filter = mean(new_particles, 2);
                        
                        position_particles = new_particles;
                        
                        %  Correct current position based on position estimation                       
                        pos_delta(1:3) = pos - position_particle_filter(1:3)';
                        pos = pos - pos_delta * DELTA_PARAM;
                                              
                        %   RMS error of the distance between corrected and original trajectory
                        RMS_counter = RMS_counter + 1;
                        
                        temp = (ground_truth(k,2)-odom(k,2))^2 + (ground_truth(k,3)-odom(k,3))^2 + (ground_truth(k,4)-odom(k,4))^2;
                        RMS_pos_uncor_temp = RMS_pos_uncor_temp+ temp;
                        RMS_pos_uncor(RMS_counter) = sqrt(RMS_pos_uncor_temp/RMS_counter);
                                  
                        temp = (ground_truth(k,2)-pos(1))^2 + (ground_truth(k,3)-pos(2))^2 + (ground_truth(k,4)-pos(3))^2 ;
                        RMS_pos_cor_temp = RMS_pos_cor_temp + temp;
                        RMS_pos_cor(RMS_counter) = sqrt(RMS_pos_cor_temp/RMS_counter);
                   
                        %RMS error for old and corrected yaw
                        reference_eul = quat_2_euler(ground_truth(k,5:8)');
                        temp = wrapToPi(reference_eul(3) - corrected_eul(3));
                        temp = temp^2;  
                        RMS_yaw_cor_temp = RMS_yaw_cor_temp + temp;
                        RMS_yaw_cor(RMS_counter) = sqrt(RMS_yaw_cor_temp/RMS_counter);
                            
                        temp = wrapToPi(reference_eul(3) - odom_eul(3));
                        temp = temp^2;
                        RMS_yaw_uncor_temp = RMS_yaw_uncor_temp + temp;
                        RMS_yaw_uncor(RMS_counter) = sqrt(RMS_yaw_uncor_temp/RMS_counter);
                        
                        odom_data_pointer=k;
                        break;
                    end
                end                                        
            end
            break;
        end
    end
end

figure(2);
plot(1:RMS_counter,RMS_pos_cor(:),1:RMS_counter,RMS_pos_uncor(:),'r--');
hold on;
grid on;
title('RMS error of trajectory');
legend('RMS error of corrected odometry','RMS error of plain odometry');
xlabel('Time');
ylabel('RMS error [m]');

if CORRECT_YAW
    figure(3);
    plot(1:RMS_counter, RMS_yaw_cor(:),1:RMS_counter, RMS_yaw_uncor(:),'r--');
    hold on;
    grid on;
    title('RMS error of yaw');
    legend('RMS error of corrected yaw','RMS error of odometry yaw');
    xlabel('"Time"');
    ylabel('RMS error [rad]');
end