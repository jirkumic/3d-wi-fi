%Computes and plots the probability of the vehicle position from X(NUM_WIFI_LOGS)
%measurement and computes RMS for position and yaw. 
%Yaw is corrected using least square method from $YAW_LS_POINTS selected particle means

clear, close all
me = mfilename;
mydir = which(me); mydir = mydir(1:end-2-numel(me)); 
addpath(genpath(mydir)); 

wifilog_name = 'wifilog201672.csv';      %wifilog
odom_file_name = 'insorob201672.csv';    %insorob/odometry for given wifilog
load('ground_truth201672.mat');          %ground truth for given wifilog and insorob/odometry
load('GP_wifi_map201671.mat');           %created location GP map
CHANGE_COORD = 0;                       %set to 1 if the trajectory is in wrong direction for ex. ground truth 20151, 20161
CORRECT_YAW = 1;                        %set to 1 to correct yaw or to 0 to not correct yaw

wifilog = readtable(wifilog_name,'Delimiter',',','ReadVariableNames',false);
odom = importdata(odom_file_name,',');

YAW_LS_POINTS = 4;              %number of corrected inso trajectory points for yaw estimation
NUM_WIFI_LOGS = 20;             %number of wifi RSSI measurements to compute the probability from
YAW_ERR_PARAM = 0.2;            %This parameter defines how much will be yaw error corrected based on yaw approximation
DELTA_PARAM = 0.04;             %This parameter defines how much will be odometry corrected based on position approximation
GP_RANGE_X = 8;                 %Range on X axis for position estimation 
GP_RANGE_Y = GP_RANGE_X;        %Range on Y for position estimation
GP_RANGE_Z = 2.5;               %Range on Z for position estimation

% initial values
wifilog_counter = 0;
odom_data_pointer = 1;
corrected_odom = zeros(length(odom)-1, 4);
traveled_dist = 0;
pos = [0,0,0];

yaw_LS_counter = 1;
direction_point_counter = 1;

yaw_error = 0;
odom_eul = [0,0,0];
odom_quat = [0,0,0,0];
corrected_eul = [0,0,0];
corrected_quat = [0,0,0,0];

RMS_counter = 0;

RMS_pos_cor = 0;
RMS_pos_uncor = 0;
RMS_pos_cor_temp = 0;
RMS_pos_uncor_temp = 0;

RMS_yaw_cor = 0;
RMS_yaw_uncor = 0;
RMS_yaw_cor_temp = 0;
RMS_yaw_uncor_temp = 0;

odom_initial_prior_sigma = 100 * eye(3);
odom_user_prior_sigma = 5 * eye(3);   % how sharp is the prior gaussian distribution
odom_prior_sigma = odom_initial_prior_sigma;

meanfunc = @meanZero;
covfunc = @covSEard;
likfunc = @likGauss;

[Xtest1, Xtest2, Xtest3] = meshgrid(-25:1:15,-10:1:30,-7:0.2:7);
Xtest = [Xtest1(:) Xtest2(:) Xtest3(:)];

%% Changing x and y axes directions (If needed)

if CHANGE_COORD
    ground_truth(:,2) = -ground_truth(:,2);
    ground_truth(:,3) = -ground_truth(:,3);

    odom(:,2) = -odom(:,2);
    odom(:,3) = -odom(:,3);
end

odom_diff_body = zeros(size(odom,1),3);
icp_diff_world = diff(ground_truth(:,2:4)',1,2);

%get inso distance increments 
for i=2:length(odom)
    odom_diff_m = odom(i,2:4) - odom(i-1,2:4);  %in the world frame
    odom_diff_body(i,:) = quat_2_dcm(odom(i-1,5:8)') * odom_diff_m'; %in the body f.
end

wifilog_time = wifilog{:,{'Var1'}};

% plot original odometry and ground truth
figure(1);
plot3(odom(:,2),odom(:,3),odom(:,4),'g')
hold on; axis equal;
plot3(ground_truth(:,2),ground_truth(:,3),ground_truth(:,4),'b')
xlabel('X-axis [m]');
ylabel('Y-axis [m]');
zlabel('Z-axis [m]');
legend('Odometry','Ground truth', 'Corrected odometry','Position estimates')

%% generate prior constraint map
prior_map = generate_prior_map([ground_truth(:,2),ground_truth(:,3),ground_truth(:,4)]);

RSSI = table2array(wifilog(:,3));

% RSSI buffers
rssi_measurements_buffer = {};
rssi_measurements_buffer_pointer = 1;

%% MAIN LOOP
for i=1:size(wifilog,1)  % over all wifi packets
    % current wifi pos time
    packet_time = wifilog_time(i);

    % avoid  packets that arrived before odometry measurement started
    if packet_time < odom(1,1)
        continue;
    end
    for j=1:size(GP_wifi_map.MAC,1)  % over all known AP MACs
        if strcmp(wifilog{i,{'Var2'}},GP_wifi_map.MAC(j))  % if given packet matches MAC
            formatSpec = '%d out of %d, MAC %d \n';
            %fprintf(formatSpec,i,size(wifilog,1),j);
            wifilog_counter = wifilog_counter + 1;               % how many packets do we have at the moment

            % Store the values to some buffer  
            rssi_measurements_buffer{rssi_measurements_buffer_pointer}.j = j;
            rssi_measurements_buffer{rssi_measurements_buffer_pointer}.Ytest = RSSI(i) + 100;
            rssi_measurements_buffer_pointer = rssi_measurements_buffer_pointer + 1;
            
            %% if enough packets already, use them
            if mod(wifilog_counter,NUM_WIFI_LOGS) == 0                                              
                % odometry correction and RMS error
                for k = odom_data_pointer+1:1:length(odom)-1
                    
                    % next position according to distance increament and
                    % added yaw error
                    if k == 1
                        pos = odom(k, 2:4);
                    else
                        odom_quat = odom(k,5:8);
                        odom_eul = quat_2_euler(odom_quat');
                        corrected_eul = wrapToPi(odom_eul + [ 0 ; 0; yaw_error]);                        
                        corrected_quat = euler_2_quat(corrected_eul);
                        
                        pos = pos + ...
                            (quat_2_dcm(corrected_quat)'*odom_diff_body(k,:)')';
                     end
                    
                    % copy original time
                    corrected_odom(k,1) = odom(k,1);
                    % save last position estimation
                    corrected_odom(k,2:4) = pos;
                    traveled_dist = traveled_dist + sqrt(icp_diff_world(1,k)^2 + icp_diff_world(2,k)^2 + icp_diff_world(3,k)^2);

                    %save x and y pos for computating the yaw
                    direction_point_vector(direction_point_counter,1) = pos(1);
                    direction_point_vector(direction_point_counter,2) = pos(2);
                    direction_point_counter = direction_point_counter + 1;
                    
                    % if reached wifi time, compute new delta and brake
                    if odom(k+1,1) >= packet_time &&  odom(k,1) <= packet_time
                         
                        if CORRECT_YAW                       
                            %compute yaw error from last $yaw_LS_counter corrected
                            %odometry points
                            if yaw_LS_counter < YAW_LS_POINTS
                                yaw_LS_counter = yaw_LS_counter + 1;
                            else
                                yaw_LS_counter = 0;
                                [yaw_difference, add] = leastSquareYaw(direction_point_vector, ...
                                    corrected_eul(3), CHANGE_COORD);
                                direction_point_counter = 1;
                                clearvars direction_point_vector;

                                if add
                                    %difference(yaw error) between estimated yaw and odometry yaw                              
                                    yaw_error = wrapToPi(yaw_error - yaw_difference * YAW_ERR_PARAM);
                                    
                                end                           
                            end
                        end
                        
                        % Go through the buffered measurements
                        % and use them to evaluate the log likelihood
                        log_likelihoods = 0;
                        for buff_ptr = 1:(rssi_measurements_buffer_pointer-1)
                            jj = rssi_measurements_buffer{buff_ptr}.j;
                            Ytest(1:size(Xtest,1)) =  rssi_measurements_buffer{buff_ptr}.Ytest;
                            x = [GP_wifi_map.data(jj).x, GP_wifi_map.data(jj).y, GP_wifi_map.data(jj).z];
                            rssi = GP_wifi_map.data(jj).RSSI;
                            hyp = GP_wifi_map.hyp(jj);          
                            [ymu, ys2, fmu, fs2, lp] = ...
                            gp(hyp, @infExact, meanfunc, covfunc, likfunc, ...
                               x, rssi, Xtest, Ytest.');
                            log_likelihoods = log_likelihoods + lp;
                        end
                        
                        % clean the buffer
                        rssi_measurements_buffer = {};
                        rssi_measurements_buffer_pointer = 1;
                        
                        % the prior mean
                        odom_prior_mu = corrected_odom(k, 2:4);
                        
                        % the prior distribution
                        prior_dist = log(mvnpdf_non_lic(Xtest, odom_prior_mu, odom_prior_sigma));
                                                
                        % overwrite the initial non-informative sigma by user-set one
                        odom_prior_sigma = odom_user_prior_sigma;
                        
                        % compute the constraint prior
                        prior_constraint_dist = get_prior_constraint_log(Xtest, prior_map);
                        
                        % posterior distribution
                        posterior_dist = prior_dist + prior_constraint_dist + log_likelihoods;
                        
                        % find maximum of posterior
                        [M,I] = max(posterior_dist);          
                        
                        position_GP = Xtest(I,:);
                                                
                        plot3(corrected_odom(odom_data_pointer:k,2),corrected_odom(odom_data_pointer:k,3),corrected_odom(odom_data_pointer:k,4),'k','LineWidth',3);
                        plot3(position_GP(1),position_GP(2),position_GP(3),'r*');
                        drawnow;
                        
                        %  Correct current position based on position estimation
                        
                        pos_delta(1:3) = pos - position_GP;
                        pos = pos - pos_delta * DELTA_PARAM;
                                              
                        %   RMS error of the distance between corrected and original trajectory
                        RMS_counter = RMS_counter + 1;
                        
                        temp = (ground_truth(k,2)-odom(k,2))^2 + (ground_truth(k,3)-odom(k,3))^2 + (ground_truth(k,4)-odom(k,4))^2;
                        RMS_pos_uncor_temp = RMS_pos_uncor_temp+ temp;
                        RMS_pos_uncor(RMS_counter) = sqrt(RMS_pos_uncor_temp/RMS_counter);
                                  
                        temp = (ground_truth(k,2)-pos(1))^2 + (ground_truth(k,3)-pos(2))^2 + (ground_truth(k,4)-pos(3))^2 ;
                        RMS_pos_cor_temp = RMS_pos_cor_temp + temp;
                        RMS_pos_cor(RMS_counter) = sqrt(RMS_pos_cor_temp/RMS_counter);
                   
                        %RMS error for old and corrected yaw
                        reference_eul = quat_2_euler(ground_truth(k,5:8)');
                        temp = wrapToPi(reference_eul(3) - corrected_eul(3));
                        temp = temp^2;
                        RMS_yaw_cor_temp = RMS_yaw_cor_temp + temp;
                        RMS_yaw_cor(RMS_counter) = sqrt(RMS_yaw_cor_temp/RMS_counter);

                        temp = wrapToPi(reference_eul(3) - odom_eul(3));
                        temp = temp^2;
                        RMS_yaw_uncor_temp = RMS_yaw_uncor_temp + temp;
                        RMS_yaw_uncor(RMS_counter) = sqrt(RMS_yaw_uncor_temp/RMS_counter);
                        
                        odom_data_pointer=k;
                        break;
                    end
                end
                %compute new Xtest area coordinates
                [Xtest1, Xtest2, Xtest3] = meshgrid(pos(1)-GP_RANGE_X:1:pos(1) + GP_RANGE_X, ...
                    pos(2)-GP_RANGE_Y:1:pos(2)+GP_RANGE_Y, pos(3)-GP_RANGE_Z:0.2:pos(3)+GP_RANGE_Z); %only nearby area                 
                Xtest = [Xtest1(:) Xtest2(:) Xtest3(:)];
            end
            break;
        end
    end
end

figure(2);
plot(1:RMS_counter,RMS_pos_cor(:),1:RMS_counter,RMS_pos_uncor(:),'r--');
hold on;
grid on;
title('RMS error of trajectory');
legend('RMS error of corrected odometry','RMS error of plain odometry');
xlabel('Time');
ylabel('RMS error [m]');

if CORRECT_YAW
    figure(3);
    plot(1:RMS_counter, RMS_yaw_cor(:),1:RMS_counter, RMS_yaw_uncor(:),'r--');
    hold on;
    grid on;
    title('RMS error of yaw');
    legend('RMS error of corrected yaw','RMS error of odometry yaw');
    xlabel('"Time"');
    ylabel('RMS error [rad]');
end