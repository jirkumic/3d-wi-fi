clear all; close all;
me = mfilename;
mydir = which(me); mydir = mydir(1:end-2-numel(me)); 
addpath(genpath(mydir));

meanfunc = @meanZero;
covfunc = @covSEard;
likfunc = @likGauss;
load('wifilog_with_position201622.mat');       

GP_wifi_map = struct;
GP_wifi_map.MAC=sort(unique(wifilog_with_position.MAC));
for i=1:size(GP_wifi_map.MAC)
    [x, y, z, RSSI] =pairCoorRSSI(GP_wifi_map.MAC(i),wifilog_with_position);
    GP_wifi_map.data(i,1).x = x';
    GP_wifi_map.data(i,1).y = y';
    GP_wifi_map.data(i,1).z = z';
    GP_wifi_map.data(i,1).RSSI = RSSI';
    
    hyp.cov = [0; 0; 0; 0];
    hyp.lik = log(0.1);
    
    X = [x',y',z'];
    hyp = minimize(hyp, @gp, -100, @infExact, meanfunc, covfunc, likfunc, X, RSSI');
    fprintf('MAC n.%d out of %d done\n',i, size(GP_wifi_map.MAC));
    GP_wifi_map.hyp(i,1)= hyp;
end
