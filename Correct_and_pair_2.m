%Creates ground truth and pairs it with wifilog using corrected icp.

clear all; close all;
me = mfilename;
mydir = which(me); mydir = mydir(1:end-2-numel(me)); 
addpath(genpath(mydir));

insorob_name = 'insorob201671.csv';
load('icpCor201671.mat');
wifilog_name = 'wifilog201671.csv';

minimum_RSSI_logs = 10;                           %minimum RSSI logs for each MAC address

ground_truth = importdata(insorob_name,',');      %plain odometry is loaded before correcting it to ground truth

plot3(ground_truth(:,2),ground_truth(:,3),ground_truth(:,4),'g')

%% inso corrected by icp
k=1;        
for i=1:1:length(icp_corrected)-1
    valueP1 = icp_corrected(i,:);
    valueP2 = icp_corrected(i+1,:);
    for j=k:1:length(ground_truth)
        if ground_truth(j,1) >= valueP1(1)
            for k=j:1:length(ground_truth)
                if ground_truth(k,1) >= valueP2(1)
                    %reference position correction
                    delta(1:3) =  valueP1(2:4)-ground_truth(j,2:4);
                    for n=j:1:k
                        ground_truth(n,2:4)= ground_truth(n,2:4) + delta(1:3);
                    end
                    %linear correction
                    for n=j+1:1:k                      
                        ground_truth(n,2:4)= valueP1(2:4)+((n-j)/(k-j))*(valueP2(2:4)-valueP1(2:4));
                    end                    
                    break;
                end
            end
            break;
        end
    end
end

%correcting rest of the inso data
delta(1:3) =  valueP2(2:4)+(1/(k-j))*(valueP1(2:4)-valueP2(2:4))-ground_truth(k+1,2:4);
for n=k+1:1:length(ground_truth)
    ground_truth(n,2:4)=ground_truth(n,2:4)+ delta(1:3);
end

grid on;
hold on;
plot3(icp_corrected(:,2),icp_corrected(:,3),icp_corrected(:,4),'r')
plot3(ground_truth(:,2),ground_truth(:,3),ground_truth(:,4),'b')
title('Paths');
legend('Odometry trajectory','Icp trajectory','Corrected odometry trajectory');
xlabel('X-Position');ylabel('Y-Position');zlabel('Z-Position');

%plot reference and odometry yaw
for i =1:1:length(ground_truth)
    gt_eul(i,:) = quat_2_euler(ground_truth(i,5:8)');
    odom_eul(i,:) = quat_2_euler(odom(i,5:8)');
end

figure(2)
grid on;
hold on;
plot(odom_eul(:,3), 'r')
plot(gt_eul(:,3),'b')
title('Yaw');
legend('Odometry yaw','Reference yaw');

%% Pairing (takes lot of time)
wifilog = filterMAC(wifilog_name, minimum_RSSI_logs);
wifilog_with_position=struct;
time=wifilog{:,{'Var1'}};
MAC=wifilog{:,{'Var2'}};
RSSI=wifilog{:,{'Var3'}};
k=1;
pairCounter = 1;
for i=1:1:size(wifilog,1)
    for j=k:1:size(ground_truth)-1
        if time(i)<= ground_truth(j+1,1) && time(i)>= ground_truth(j,1)
            if abs(time(i)-ground_truth(j+1))<= (time(i)-ground_truth(j))
               wifilog_with_position.time(pairCounter,1)=time(i);
               wifilog_with_position.MAC(pairCounter,1)=MAC(i);
               wifilog_with_position.RSSI(pairCounter,1)=RSSI(i)+100;              %from -100, -1 dbm  to 0 , 99; 0 = almost no or no signal 
               wifilog_with_position.pos(pairCounter,1:3)=ground_truth(j+1,2:4);
               k=j;
               pairCounter = pairCounter +1;
               break;
            else
               wifilog_with_position.time(pairCounter,1)=time(i);
               wifilog_with_position.MAC(pairCounter,1)=MAC(i);
               wifilog_with_position.RSSI(pairCounter,1)=RSSI(i)+100;              %from -100, -1 dbm  to 0 , 99; 0 = almost no or no signal
               wifilog_with_position.pos(pairCounter,1:3)=ground_truth(j,2:4);
               k=j;
               pairCounter = pairCounter +1;
               break;
            end
        end
    end
end