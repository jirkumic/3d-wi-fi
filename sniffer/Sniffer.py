#!/usr/bin/env python
from scapy.all import *
from scapy.layers.dot11 import *
import csv
import time


 
def PacketHandler(pkt) :

    if pkt.haslayer(Dot11) :
        if pkt.type == 0 and pkt.subtype == 8 :
                try:
                    extra = pkt.notdecoded
                except:
                    extra = None
                if extra!=None:
                    signal_strength = -(256-ord(extra[-6:-5]))
                    #for c in extra: -6 a -5
                    #    print ord(c)
                else:
                     signal_strength = -100
                print signal_strength
                logwriter.writerow([time.time() , pkt.addr2, signal_strength])


#sudo airmon-ng start wlan1  for turning on the monitor mode on external wifi reciever
f=open('wifilog.csv', 'wb')
try:
    logwriter = csv.writer(f , delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
    sniff(iface = "wlan1", prn = PacketHandler)
finally:
    f.close()
