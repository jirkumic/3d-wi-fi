#!/bin/bash

ifconfig wlan1 down
sleep 0.1
iwconfig wlan1 mode monitor
sleep 0.1
ifconfig wlan1 up
sleep 0.1

x=1

while true; do
		
	if [ $x -eq 14 ] ; then		
		x=1	
	fi 
	
        iwconfig wlan1 channel $x
	echo $x
	sleep 0.1
	x=$[$x+1]
done
