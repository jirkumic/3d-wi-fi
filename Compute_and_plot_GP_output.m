%Computes and plots the probability of the vehicle position from X(NUMLOG)
%measurement.

clear all, close all
me = mfilename;
mydir = which(me); mydir = mydir(1:end-2-numel(me)); 
addpath(genpath(mydir));

wifilog = 'wifilog20161x.csv';

T = readtable(wifilog,'Delimiter',',','ReadVariableNames',false);

load('Map20163.mat');
load('inso201622.mat');

NUMLOG = 40;                    %number of measurements to compute the probability from
XRange = 4;                     %area around X
YRange = XRange;                %area around Y
ZRange = 0.8;                   %area around Z

INSO = INSO(:, 2:4);
a = INSO(:,1);
b = INSO(:,2);
c = INSO(:,3);

RSSI = table2array(T(:,3));
plot3(a,b,c,'k')
meanfunc = @meanZero;
covfunc = @covSEard;
likfunc = @likGauss;
[Xtest1, Xtest2, Xtest3] = meshgrid(-25:1:15, -25:1:5,-2:0.2:6);
Xtest = [Xtest1(:) Xtest2(:) Xtest3(:)];
tempLp = 0;
count=0;
for i=1:size(T)
    for j=1:size(out.MAC)
        if strcmp(T{i,{'Var2'}},out.MAC(j))
            fprintf('%d out of %d, MAC %d\n',i,size(T),j)
            count = count + 1;
            x= [out.data(j).x,out.data(j).y, out.data(j).z];
            rssi= out.data(j).rssi;
            Ytest(1:size(Xtest,1))= RSSI(i)+100;
            hyp=out.hyp(j);
            [ymu, ys2, fmu, fs2, lp] = gp(hyp, @infExact, meanfunc, covfunc, likfunc, x, rssi, Xtest,Ytest.');
            tempLp=tempLp+lp;
            if mod(count,NUMLOG) == 0               
                n= count/NUMLOG;
                [M,I] = max(tempLp);
                X(n,1) = Xtest(I,1);
                Y(n,1) = Xtest(I,2);
                Z(n,1) = Xtest(I,3);
                tempLp = 0;
                [Xtest1, Xtest2, Xtest3] = meshgrid(X(n,1)-XRange:1:X(n,1)+XRange, Y(n,1)-YRange:1:Y(n,1)+YRange,Z(n,1)-ZRange:0.2:Z(n,1)+ZRange);
                Xtest = [Xtest1(:) Xtest2(:) Xtest3(:)];
                grid on
                hold on
                drawnow
                plot3(X,Y,Z,'r*')
            end
            break
        end
    end
end
