This README file describes structure of the .csv files created 
by the INSO INSO.LAUNCH or by the CTU_DATA_LOGGER INSO_LOGGER.LAUNCH.


ICP files:   inso_icp_XX_XX_XXXX_XXhXXmXXs.csv
----------------------------------------------
----------------------------------------------
Warning: Recently ( since 2013/01/30 ), there was a change in the ICP data meaning. 
Before, the position and attitude stored in the icp files resembled correction of the current position 
indicated by inso. Fromt the date, position and attitude correspond with the absolute position
in the /map coordinate frame.
However, the structure of the file is the same:



time,    pos_x, pos_y, pos_z,       att_x, att_y, att_z, att_w,     vel_x, vel_y, vel_z,    ang_rate_x, ang_rate_y, ang_rate_z
...
...



The units are: time=[s], pos=[m], att = quaternion, vel=[m/s], ang_rate=[rad/s].
The values are the same that are published in the messages, however, velocities and angular rates are zero since they are published this way.






Magnetometer files:   inso_magnetic_XX_XX_XXXX_XXhXXmXXs.csv
------------------------------------------------------------
------------------------------------------------------------
The values are a direct copy of the magnetometer sensor readings published by the robot driver:



time  mag_x mag_y mag_z
...
...


The units are: time=[s], mag=[T].
There are no commas, sorry for the inconsistency.





   
Mechanization files:   inso_mech_XX_XX_XXXX_XXhXXmXXs.csv
---------------------------------------------------------
---------------------------------------------------------
These contain input data the inso uses, output angles and XSENS output angles. For the experiments that contain icp or vodom, the 
inso angles are zero since these files are not created by inso anymore but by a special logger, which doesn't have a direct acces to
the inso variables. The file structure is as follows:


time, acc_x, acc_y, acc_z, gyr_x, gyr_y, gyr_z, roll_inso, pitch_inso, yaw_inso, pitch_xsens, roll_xsens, yaw_xsens
...
...

Units are time=[s], acc=[m/(s.s)], gyr=[rad/s], roll/pitch/yaw = [degrees].
Super important: the values are expressed in the NED coordinate frame. To work in the ROS coordinate frame convention, which is NWU,
a transformation [1 0 0; 0 -1 0; 0 0 -1] must be used. I.e. if it behaves strangely, check axes.








Track velocities files:   inso_tracks_XX_XX_XXXX_XXhXXmXXs.csv
--------------------------------------------------------------
--------------------------------------------------------------
These values are velocity measurements from the caterpillar tracks, note they are quite noisy...


time vel_left vel_right
...
...

Units: time=[s], vel=[m/s]
No commas again, sorry.
 





VODOM files:   inso_vodom_XX_XX_XXXX_XXhXXmXXs.csv
----------------------------------------------
----------------------------------------------
The visual odometry files format is identical to the ICP files format:



time,    pos_x, pos_y, pos_z,       att_x, att_y, att_z, att_w,     vel_x, vel_y, vel_z,    ang_rate_x, ang_rate_y, ang_rate_z
...
...



The units are: time=[s], pos=[m], att = quaternion, vel=[m/s], ang_rate=[rad/s].
The velocities are zero again, it's being published this way.
