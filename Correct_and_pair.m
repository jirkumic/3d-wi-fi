%Creates ground truth and pairs wifilog with it if no corrected icp is provided. 

clear all; close all;
me = mfilename;
mydir = which(me); mydir = mydir(1:end-2-numel(me)); 
addpath(genpath(mydir));

insorob_name = 'insorob201671.csv';
icp_name = 'icp201671.csv';
wifilog_name = 'wifilog201671.csv';

minimum_RSSI_logs = 10;                          %minimum RSSI logs for each MAC address
tilt = 0.06;                         %tilt on the stairs
floorVar = 1.3;                      %range for the floor recognition
floor0 = -5;                         % z position of ground floor
floor1 = 0;
floor2 = 5;

odom = importdata(insorob_name,',');    %plain odometry is loaded before correcting it to ground truth
icp = importdata(icp_name,',');
ground_truth = odom;
plot3(odom(:,2),odom(:,3),odom(:,4),'g')

%% inso corrected by icp
k=1;        
for i=1:1:length(icp)-1
    valueP1 = icp(i,:);
    valueP2 = icp(i+1,:);
    for j=k:1:length(ground_truth)
        if ground_truth(j,1) >= valueP1(1)
            for k=j:1:length(ground_truth)
                if ground_truth(k,1) >= valueP2(1)
                    %reference position and quaternion correction
                    delta(1:7) =  valueP1(2:8)-ground_truth(j,2:8);
                    for n=j:1:k
                        ground_truth(n,2:8)= ground_truth(n,2:8) + delta(1:7);
                    end
                    %linear correction
                    for n=j+1:1:k                      
                        ground_truth(n,2:8)= valueP1(2:8)+((n-j)/(k-j))*(valueP2(2:8)-valueP1(2:8));
                    end                    
                    break;
                end
            end
            break;
        end
    end
end

%correcting rest of the inso data
delta(1:7) =  valueP2(2:8)+(1/(k-j))*(valueP1(2:8)-valueP2(2:8))-ground_truth(k+1,2:8);
for n=k+1:1:length(ground_truth)
    ground_truth(n,2:8)=ground_truth(n,2:8)+ delta(1:7);
end

%%
%Z position correction, minor improvements needed
k=1;
sw=1;
err=0;
for i=1:1:length(ground_truth)
    
    if ground_truth(i,6) < tilt && ground_truth(i,6) > -tilt && floor0 - ground_truth(i,4) > -floorVar && floor0 - ground_truth(i,4) < floorVar
        ground_truth(i,4) = floor0;
        if sw ~= 1
            sw = 2;
        end;
    elseif ground_truth(i,6) < tilt && ground_truth(i,6) > -tilt && floor1 - ground_truth(i,4) > -floorVar && floor1 - ground_truth(i,4) < floorVar
        ground_truth(i,4) = floor1;
        if sw ~= 1
            sw = 2;
        end;
    elseif ground_truth(i,6) < tilt && ground_truth(i,6) > -tilt && floor2 - ground_truth(i,4) > -floorVar && floor2 - ground_truth(i,4) < floorVar
        ground_truth(i,4) = floor2;
        if sw ~= 1
            sw = 2;
        end;
    %going up/down stairs
%     elseif i-1>0
%         if sw==2;
%             err = ground_truth(i-1,4)-ground_truth(i,4);
%             sw=0;
%         end
%         ground_truth(i,4)=ground_truth(i,4)-err;
%     end

    elseif sw == 1 && i-1>0             
        k = i-1;
        sw=0; 
    end
    
    if sw==2
       sw=1;
       zErrIn=ground_truth(k+1,4)-ground_truth(k,4);
       zErrOut = ground_truth(i-1,4)-ground_truth(i,4);
       for j=k+1:1:i-1
           ground_truth(j,4)= ground_truth(j,4)-zErrIn -(j-k)*zErrOut/(i-k);
       end
    end
end

grid on;
hold on
plot3(icp(:,2),icp(:,3),icp(:,4),'r')
plot3(ground_truth(:,2),ground_truth(:,3),ground_truth(:,4),'b')
title('Paths');
legend('Odometry trajectory','Icp trajectory','Corrected odometry trajectory');
xlabel('X-Position');ylabel('Y-Position');zlabel('Z-Position');

%plot reference and odometry yaw
for i =1:1:length(ground_truth)
    gt_eul(i,:) = quat_2_euler(ground_truth(i,5:8)');
    odom_eul(i,:) = quat_2_euler(odom(i,5:8)');
end


figure(2)
grid on;
hold on;
plot(odom_eul(:,3), 'r')
plot(gt_eul(:,3),'b')
title('Yaw');
legend('Odometry yaw','Reference yaw');

%% Pairing (takes lot of time)
wifilog = filterMAC(wifilog_name, minimum_RSSI_logs);
wifilog_with_position=struct;
time=wifilog{:,{'Var1'}};
MAC=wifilog{:,{'Var2'}};
RSSI=wifilog{:,{'Var3'}};
k=1;
pairCounter = 1;
for i=1:1:size(wifilog,1)
    for j=k:1:size(ground_truth)-1
        if time(i)<= ground_truth(j+1,1) && time(i)>= ground_truth(j,1)
            if abs(time(i)-ground_truth(j+1))<= (time(i)-ground_truth(j))
               wifilog_with_position.time(pairCounter,1)=time(i);
               wifilog_with_position.MAC(pairCounter,1)=MAC(i);
               wifilog_with_position.RSSI(pairCounter,1)=RSSI(i)+100;              %from -100, -1 dbm  to 0 , 99; 0 = almost no or no signal 
               wifilog_with_position.pos(pairCounter,1:3)=ground_truth(j+1,2:4);
               k=j;
               pairCounter = pairCounter +1;
               break;
            else
               wifilog_with_position.time(pairCounter,1)=time(i);
               wifilog_with_position.MAC(pairCounter,1)=MAC(i);
               wifilog_with_position.RSSI(pairCounter,1)=RSSI(i)+100;              %from -100, -1 dbm  to 0 , 99; 0 = almost no or no signal
               wifilog_with_position.pos(pairCounter,1:3)=ground_truth(j,2:4);
               k=j;
               pairCounter = pairCounter +1;
               break;
            end         
        end      
    end
end