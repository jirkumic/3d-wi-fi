%Combine two or more experiments in order to create more generalized GP wifi map
clear all;
data1 = load('wifilog_with_position20163.mat');
data2 = load('wifilog_with_position201622.mat');

icp1 = load('icpCor20163.mat');
icp2 = load('icpCor201622.mat');

plot3(icp1.icp_corrected(:,2),icp1.icp_corrected(:,3),icp1.icp_corrected(:,4))
hold on;
grid on;
plot3(icp2.icp_corrected(:,2),icp2.icp_corrected(:,3),icp2.icp_corrected(:,4))



wifilog_with_position.time = [data1.wifilog_with_position.time ; data2.wifilog_with_position.time];
wifilog_with_position.MAC = [data1.wifilog_with_position.MAC ; data2.wifilog_with_position.MAC];
wifilog_with_position.RSSI = [data1.wifilog_with_position.RSSI ; data2.wifilog_with_position.RSSI];
wifilog_with_position.pos = [data1.wifilog_with_position.pos ; data2.wifilog_with_position.pos];