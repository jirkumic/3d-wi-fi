function [ q ] = rotv_2_quat( rotv )
%ROTV2QUAT Express rotation vector as a quaternion
%   The rotation vector is expected Theta = [x ; y ; z]
%   The resulting quaternion is composed as follows:q = [vector ; scalar]


    theta = norm(rotv);   % get the angle
    
    if theta < realmin
        q = [0 ; 0 ; 0; 1];
    
    else
    
    rotv = rotv / theta;  % normalize the rot. axis
    q = [sin(theta/2)*rotv;...
         cos(theta/2) ];
    end
end

