function qout = quat_normalize( q )
%  QUATNORMALIZE Normalize a quaternion.


qout = q./(quat_mod( q )* ones(4,1));
