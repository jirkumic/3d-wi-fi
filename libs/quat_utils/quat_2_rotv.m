function [ rotv ] = quat_2_rotv( dq )
%QUAT_2_ROTV Expresses quaternion rotatiion as a rotation vector

rot_mag = 2 * asin(norm(dq(1:3)));
if(rot_mag < realmin)
    rotv = [0;0;0];
    return;
end
rot_axis = dq(1:3)/norm(dq(1:3));
rotv = rot_axis * rot_mag;

end

