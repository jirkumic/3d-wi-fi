function qout = quat_conj( qin ) 
%  QUATCONJ Calculate the conjugate of a quaternion.

qout = [ -qin(1:3) ; qin(4) ];
