function [ q_out ] = quat_multiply_JPL( q, p )
%QUAT_MULTIPLY computes quaternion multiplication q x p
%   This multiplication follows JPL Proposed Standard Convention
%   The quaternion is structured [q_vector , q_scalar]' i.e. [x y z w]'


if any(~isreal(q) || ~isnumeric(q))
    error('Input elements of q are not real numbers.');
end

if (size(q,1) ~= 4 || size(q,2) ~= 1)
    error('Input dimension of q is not 4-by-1.');
end

if any(~isreal(p) || ~isnumeric(p))
    error('Input elements of p are not real numbers.');
end

if (size(p,1) ~= 4 || size(p,2) ~= 1)
    error('Input dimension of p is not 4-by-1.');
end



q1 = q(1);
q2 = q(2);
q3 = q(3);
q4 = q(4);

p1 = p(1);
p2 = p(2);
p3 = p(3);
p4 = p(4);

q_out = [ q4*p1 + q3*p2 - q2*p3 + q1*p4 ;...
         -q3*p1 + q4*p2 + q1*p3 + q2*p4 ;...
          q2*p1 - q1*p2 + q4*p3 + q3*p4 ;...
         -q1*p1 - q2*p2 - q3*p3 + q4*p4      ];



end

