function mod = quat_mod( q )
%  QUATMOD Calculate the modulus of a quaternion.


mod = sqrt(quat_norm( q ));
