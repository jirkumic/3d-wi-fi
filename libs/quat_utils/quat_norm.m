function qout = quat_norm( q )
%  QUATNORM Calculate the norm of a quaternion.

qout = sum(q.^2,1);

