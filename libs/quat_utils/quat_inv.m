function qinv = quat_inv( q )
%  QUATINV Calculate the inverse of a quaternion.

qinv  = quat_conj( q )./(quat_norm( q )*ones(4,1));

