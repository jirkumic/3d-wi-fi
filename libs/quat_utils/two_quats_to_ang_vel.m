function [ ang_rate ] = two_quats_to_ang_vel( q1, t1, q2, t2 )
%TWO_QUATS_TO_ANG_VEL computes angular rate between rwo attitudes given
%their times



%% Angular Rate extraction

if abs(norm(q1-q2)) > abs(norm(q1+q2))
    q2 = q2*(-1);
end

dq = quat_multiply_JPL(q2, quat_inv(q1));
rot_mag = 2 * asin(norm(dq(1:3)));
if(rot_mag == 0)
    ang_rate = [0;0;0];
    return;
end
rot_axis = dq(1:3)/norm(dq(1:3));
ang_rate = rot_axis * rot_mag / (t2-t1);


end

