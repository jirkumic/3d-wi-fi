function [ theta ] = delta_quat2delta_theta( q )
%QUAT2THETA For small rotations, returns equivalent rotation vector
%   Detailed explanation goes here

    theta = 2 * q(1:3);

end

