function q = euler2quat( angles )
%EULER2QUAT Computes quaternion [x;y;z;w] from given euler angles

if any(~isreal(angles) || ~isnumeric(angles))
    error('Input elements are not real numbers.');
end

if (size(angles,1) ~= 3)
    error('Input dimension is not 3-by-M.');
end

cang = cos( angles/2 )';
sang = sin( angles/2 )';

q = [  sang(:,1).*cang(:,2).*cang(:,3) - cang(:,1).*sang(:,2).*sang(:,3), ... 
       cang(:,1).*sang(:,2).*cang(:,3) + sang(:,1).*cang(:,2).*sang(:,3), ...
       cang(:,1).*cang(:,2).*sang(:,3) - sang(:,1).*sang(:,2).*cang(:,3), ...
       cang(:,1).*cang(:,2).*cang(:,3) + sang(:,1).*sang(:,2).*sang(:,3)    ]';
