function [ OMEGA ] = omega2OMEGA( w )
%OMEGA2OMEGA Returns 4x4 OMEGA(w)
%   Matrix OMEGA is used in quaternion differential equation

if any(~isreal(w) || ~isnumeric(w))
    error('Input elements are not real numbers.');
end

if (size(w,1) ~= 3)
    error('Input dimension is not 3-by-1.');
end

wx = w(1);
wy = w(2);
wz = w(3);

OMEGA = [ 0   wz  -wy   wx;...
         -wz  0    wx   wy;...
          wy  -wx  0    wz;...
         -wx  -wy -wz   0    ];

end

