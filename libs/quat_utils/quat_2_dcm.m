function [ C ] = quat_2_dcm( q )
%QUAT2DCM Computes C rotation matrix from quaternion q [x;y;z;w]

if any(~isreal(q) || ~isnumeric(q))
    error('Input elements are not real numbers.');
end

if (size(q,1) ~= 4 || size(q,2) ~= 1)
    error('Input dimension is not 4-by-1.');
end

q = quat_normalize(q);

    C = [1-2*(q(2)^2)-2*(q(3)^2), 2*(q(1)*q(2)+q(3)*q(4)), 2*(q(1)*q(3)-q(2)*q(4));...
         2*(q(1)*q(2)-q(3)*q(4)), 1-2*(q(1)^2)-2*(q(3)^2), 2*(q(2)*q(3)+q(1)*q(4));...
         2*(q(1)*q(3)+q(2)*q(4)), 2*(q(2)*q(3)-q(1)*q(4)), 1-2*(q(1)^2)-2*(q(2)^2)...
        ];
    
end

