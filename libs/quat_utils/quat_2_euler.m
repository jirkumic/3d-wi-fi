function angles = quat_2_euler( q )
% QUAT2EULER Expresses a unit quaternion in terms of euler angles
%       q is expected to be [x y z w]'
%       angles are [roll pitch yaw]

if ~all(size(q) == [4,1])
    error('The quaternion provided as a parameter has wrong dimensions');
end
qin = quat_normalize( q );

phi = atan2(2*(qin(4)*qin(1) + qin(2)*qin(3)), ...
                 1 - 2*(qin(1)^2 + qin(2)^2));

theta = asin(2*(qin(4)*qin(2) - qin(3)*qin(1)));

psi = atan2(2*(qin(4)*qin(3) + qin(1)*qin(2)), ...
                 1 - 2*(qin(2)^2 + qin(3)^2));

angles = [phi theta psi]';
